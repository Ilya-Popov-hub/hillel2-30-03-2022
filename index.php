<?php
$a = [1, 2, 3, null, 'John'];
function inArray(array $array, $search)
{
    foreach ($array as $item) {
        if ($item === $search) {
            return true;
        }
    }

    return false;
}

var_dump(inArray($a, 'Johnn'));
